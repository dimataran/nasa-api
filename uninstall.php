<?php

if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
    exit;
if ( get_option( 'insert_5_posts' ) != false ) {
    delete_option( 'insert_5_posts' );
}
