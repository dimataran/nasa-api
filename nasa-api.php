<?php

/**
 * Plugin Name: Nasa Photo Api Plugin
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Dmitry Taran
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       nasa-api
 */

require_once 'vendor/autoload.php';

require_once 'post-types/post-nasa-gallery.php';
require_once( ABSPATH . 'wp-includes/pluggable.php' );
require_once( ABSPATH . 'wp-admin/includes/image.php' );

use Taran\NasaApi\Init;

Init::init();


//wp_clear_scheduled_hook( 'nasa_api_daily_event_hook' );
//$result = wp_schedule_event( time(), 'custom_minute', 'nasa_api_daily_event_hook');
//var_dump($result);

//$nasa_api = new \Taran\NasaApi\NasaApi( new \Taran\NasaApi\Options());
//$data = $nasa_api->getCacheJsonData('cron_post');
//$gallery = new Gallery($data);
//if ( $gallery_id = $gallery->createPost() ) {
//
//    $image = new Image($nasa_api, $data, $gallery_id);
//    $image_id = $image->createPost();
//}
//var_dump(defined('DOING_CRON')); die();

