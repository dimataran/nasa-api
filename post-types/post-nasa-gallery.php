<?php

/**
 * Registers the `post_nasa_gallery` post type.
 */
function post_nasa_gallery_init() {
	register_post_type(
		'post-nasa-gallery',
		[
			'labels'                => [
				'name'                  => __( 'Post Nasa Galleries', 'nasa-api' ),
				'singular_name'         => __( 'Post Nasa Gallery', 'nasa-api' ),
				'all_items'             => __( 'All Post Nasa Galleries', 'nasa-api' ),
				'archives'              => __( 'Post Nasa Gallery Archives', 'nasa-api' ),
				'attributes'            => __( 'Post Nasa Gallery Attributes', 'nasa-api' ),
				'insert_into_item'      => __( 'Insert into Post Nasa Gallery', 'nasa-api' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Post Nasa Gallery', 'nasa-api' ),
				'featured_image'        => _x( 'Featured Image', 'post-nasa-gallery', 'nasa-api' ),
				'set_featured_image'    => _x( 'Set featured image', 'post-nasa-gallery', 'nasa-api' ),
				'remove_featured_image' => _x( 'Remove featured image', 'post-nasa-gallery', 'nasa-api' ),
				'use_featured_image'    => _x( 'Use as featured image', 'post-nasa-gallery', 'nasa-api' ),
				'filter_items_list'     => __( 'Filter Post Nasa Galleries list', 'nasa-api' ),
				'items_list_navigation' => __( 'Post Nasa Galleries list navigation', 'nasa-api' ),
				'items_list'            => __( 'Post Nasa Galleries list', 'nasa-api' ),
				'new_item'              => __( 'New Post Nasa Gallery', 'nasa-api' ),
				'add_new'               => __( 'Add New', 'nasa-api' ),
				'add_new_item'          => __( 'Add New Post Nasa Gallery', 'nasa-api' ),
				'edit_item'             => __( 'Edit Post Nasa Gallery', 'nasa-api' ),
				'view_item'             => __( 'View Post Nasa Gallery', 'nasa-api' ),
				'view_items'            => __( 'View Post Nasa Galleries', 'nasa-api' ),
				'search_items'          => __( 'Search Post Nasa Galleries', 'nasa-api' ),
				'not_found'             => __( 'No Post Nasa Galleries found', 'nasa-api' ),
				'not_found_in_trash'    => __( 'No Post Nasa Galleries found in trash', 'nasa-api' ),
				'parent_item_colon'     => __( 'Parent Post Nasa Gallery:', 'nasa-api' ),
				'menu_name'             => __( 'Post Nasa Galleries', 'nasa-api' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title', 'editor' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => true,
			'rest_base'             => 'post-nasa-gallery',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'post_nasa_gallery_init' );

/**
 * Sets the post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `post_nasa_gallery` post type.
 */
function post_nasa_gallery_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['post-nasa-gallery'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Post Nasa Gallery updated. <a target="_blank" href="%s">View Post Nasa Gallery</a>', 'nasa-api' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'nasa-api' ),
		3  => __( 'Custom field deleted.', 'nasa-api' ),
		4  => __( 'Post Nasa Gallery updated.', 'nasa-api' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Post Nasa Gallery restored to revision from %s', 'nasa-api' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Post Nasa Gallery published. <a href="%s">View Post Nasa Gallery</a>', 'nasa-api' ), esc_url( $permalink ) ),
		7  => __( 'Post Nasa Gallery saved.', 'nasa-api' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Post Nasa Gallery submitted. <a target="_blank" href="%s">Preview Post Nasa Gallery</a>', 'nasa-api' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Post Nasa Gallery scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Post Nasa Gallery</a>', 'nasa-api' ), date_i18n( __( 'M j, Y @ G:i', 'nasa-api' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Post Nasa Gallery draft updated. <a target="_blank" href="%s">Preview Post Nasa Gallery</a>', 'nasa-api' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'post_nasa_gallery_updated_messages' );

/**
 * Sets the bulk post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `post_nasa_gallery` post type.
 */
function post_nasa_gallery_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['post-nasa-gallery'] = [
		/* translators: %s: Number of Post Nasa Galleries. */
		'updated'   => _n( '%s Post Nasa Gallery updated.', '%s Post Nasa Galleries updated.', $bulk_counts['updated'], 'nasa-api' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Post Nasa Gallery not updated, somebody is editing it.', 'nasa-api' ) :
						/* translators: %s: Number of Post Nasa Galleries. */
						_n( '%s Post Nasa Gallery not updated, somebody is editing it.', '%s Post Nasa Galleries not updated, somebody is editing them.', $bulk_counts['locked'], 'nasa-api' ),
		/* translators: %s: Number of Post Nasa Galleries. */
		'deleted'   => _n( '%s Post Nasa Gallery permanently deleted.', '%s Post Nasa Galleries permanently deleted.', $bulk_counts['deleted'], 'nasa-api' ),
		/* translators: %s: Number of Post Nasa Galleries. */
		'trashed'   => _n( '%s Post Nasa Gallery moved to the Trash.', '%s Post Nasa Galleries moved to the Trash.', $bulk_counts['trashed'], 'nasa-api' ),
		/* translators: %s: Number of Post Nasa Galleries. */
		'untrashed' => _n( '%s Post Nasa Gallery restored from the Trash.', '%s Post Nasa Galleries restored from the Trash.', $bulk_counts['untrashed'], 'nasa-api' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'post_nasa_gallery_bulk_updated_messages', 10, 2 );
