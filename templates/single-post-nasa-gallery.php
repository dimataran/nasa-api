<?php

get_header();

/* Start the Loop */
while ( have_posts() ) :
    the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header alignwide">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="alignwide">
        <figure class="post-thumbnail">
            <?php
            // Lazy-loading attributes should be skipped for thumbnails since they are immediately in the viewport.
            the_post_thumbnail( 'post-thumbnail', array( 'loading' => false ) );
            ?>
            <?php if ( wp_get_attachment_caption( get_post_thumbnail_id() ) ) : ?>
                <figcaption class="wp-caption-text"><?php echo wp_kses_post( wp_get_attachment_caption( get_post_thumbnail_id() ) ); ?></figcaption>
            <?php endif; ?>
        </figure><!-- .post-thumbnail -->
    </div>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
endwhile; // End of the loop.
get_footer();
