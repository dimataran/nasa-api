<?php

namespace Taran\NasaApi;

use WP_Query;

class Shortcodes
{

    protected $options;

    public  function __construct(Options $options)
    {
        $this->options = $options;

    }

    public function initShortcode()
    {
        add_shortcode( 'nasa_gallery', [$this , 'getNasaGallery'] );
    }


    public function getNasaGallery()
    {
        $content = '';
        $gallery_query = new WP_Query;
//        var_dump($gallery_query);
        $gallery_query->query($this->getQueryArgs());
//        var_dump($gallery_query->have_posts());
        if ($gallery_query->have_posts()) {
            ob_start();
            ?>
            <div class="nasa-image-slide">
                <?php while ($gallery_query->have_posts()) {
                $gallery_query->the_post();
                $img = $this->getPostImage(get_the_ID()); ?>

                    <div class="nasa-slide-wrap">
                        <img class="nasa-img" src="<?php echo esc_url($img); ?>"  alt="<?php the_title(); ?>" />
                    </div>

            <?php
            }
            ?>
            </div>
            <?php
            wp_reset_postdata();
            $content = ob_get_clean();
        }
        return $content;
    }


    public function getPostImage( $post_id = '', $size = 'full') {
        $size   = !empty($size) ? $size : 'full';
        $image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

        if( !empty($image) ) {
            $image = isset($image[0]) ? $image[0] : '';
        }
        return $image;
    }

    public function getQueryArgs($orderby = 'date', $order = 'DESC')
    {
        return [
            'post_type' => $this->options->getPostType(),
            'posts_per_page' => 5,
            'post_status' => 'publish',
            'orderby' => $orderby,
            'order' => $order ,
        ];
    }

}