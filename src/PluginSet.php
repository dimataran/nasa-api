<?php

namespace Taran\NasaApi;

use Taran\NasaApi\Options;
use Taran\NasaApi\NasaApi;

class PluginSet
{
    protected $nasa_api;

    public function __construct(NasaApi $nasa_api)
    {
        $this->nasa_api  = $nasa_api;
        add_filter( 'cron_schedules', [$this, 'cronAddMinute'] );
    }

    public function cronAddMinute( $schedules ) {
        $schedules['custom_minute'] = array(
            'interval' => MINUTE_IN_SECONDS,
            'display' => __( 'Minutely' )
        );
        return $schedules;
    }

    public function handleActivationSet()
    {
        wp_clear_scheduled_hook( 'nasa_api_daily_event_hook' );
        wp_schedule_event( time(), 'custom_minute', 'nasa_api_daily_event_hook');
        if (get_option('insert_5_posts') !== true) {
            $this->initialPosts();
            update_option( 'insert_5_posts', true);
        }
        add_option( 'insert_5_posts', true);



    }

    public function dailyGetPosts()
    {
       $data = $this->nasa_api->getCacheJsonData('cron_post');
       $gallery = new Gallery($data);
       if ( $gallery_id = $gallery->createPost() ) {
           $image = new Image($this->nasa_api, $data, $gallery_id);
           $image_id = $image->createPost();
       }

    }

    public function initialPosts()
    {

        $data = $this->nasa_api->getCacheRequestApi('activation_posts');

        if (! is_array($data)) {
            $data = [];
        }
        foreach ($data as $item) {
            if ( 'video' === $item->media_type ) {
                continue;
            }
            $gallery = new Gallery($item);
            if ( $gallery_id = $gallery->createPost() ) {
                $image = new Image($this->nasa_api, $item, $gallery_id);
                $image_id = $image->createPost();
            }
        }
    }

    public function handleDeactivation()
    {
        wp_clear_scheduled_hook('nasa_api_daily_event_hook');
    }


}