<?php

namespace Taran\NasaApi;

use Taran\NasaApi\Options;
use Taran\NasaApi\NasaApi;
use Taran\NasaApi\Image;
use Taran\NasaApi\Gallery;
use Taran\NasaApi\Shortcodes;

class Init
{


    private function __construct(Shortcodes $shortcodes, PluginSet $plugin_set)
    {

        register_activation_hook( dirname(__FILE__, 2) . '/nasa-api.php',  [$plugin_set, 'handleActivationSet']);
        add_action('nasa_api_daily_event_hook', [$plugin_set, 'dailyGetPosts']);
        add_action( 'wp_enqueue_scripts', [$this, 'addStyle'], 100 );
        add_filter( 'template_include', [$this, 'includeTemplate'], 1 );
        $shortcodes->initShortcode();
        register_deactivation_hook( dirname(__FILE__, 2) . '/nasa-api.php', 'handleDeactivation');

    }

    public static function init()
    {
        $options = new Options();
        $nasa_api = new NasaApi($options);
        $plugin_set = new PluginSet($nasa_api);
        $shortcodes = new Shortcodes($options);
        return new Init($shortcodes, $plugin_set);
    }

    public function includeTemplate($template_path)
    {
        if ( get_post_type() == 'post-nasa-gallery' ) {
            if ( is_single() ) { // checks if the file exists in the theme first,
                // otherwise serve the file from the plugin
                if ( $theme_file = locate_template( array
                ( 'single-post-nasa-gallery.php' ) ) ) {
                    $template_path = $theme_file;
                } else {
                    $template_path = plugin_dir_path( __FILE__ ) .
                        '../templates/single-post-nasa-gallery.php';
                }
            }
        }
        return $template_path;

    }

    public function addStyle()
    {

        wp_enqueue_style('slick',  'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', array(), null);
        wp_enqueue_style('slick',  'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', array(), null);
        wp_enqueue_style('main',  plugins_url( 'assets/css/main.css', dirname(__FILE__ ) ), array());

        wp_enqueue_script('gallery-slick',  'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], false, true);
        wp_enqueue_script('main',  plugins_url( 'assets/js/main.js', dirname(__FILE__ ) ), ['gallery-slick'], false, true);

    }




}