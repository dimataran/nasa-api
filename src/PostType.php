<?php

namespace Taran\NasaApi;

use Taran\NasaApi\NasaApi;

abstract class PostType
{

    /**
     * @var string
     */
    protected $post_type;

    /**
     * @var
     */
    protected $post_data;

    /**
     * @param $post_data
     */
    public function __construct($post_data)
    {
        $this->post_data = $post_data;
    }

    /**
     * @return string
     */
    protected function getPostType()
    {
        return $this->post_type;
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return $this->post_data->date ?? date(get_option('date_format'));
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->post_data->url ?? '';
    }

    /**
     * @return mixed
     */
    abstract protected function getQueryArgs();

    /**
     * @return mixed
     */
    abstract protected function createPost();


}