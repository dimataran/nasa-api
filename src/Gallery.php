<?php

namespace Taran\NasaApi;

use Taran\NasaApi\NasaApi;

class Gallery extends PostType
{


    /**
     * @param $post_data
     */
    public function __construct($post_data)
    {
        parent::__construct($post_data);
        $this->post_type = 'post-nasa-gallery';

    }


    /**
     * @return array
     */
    protected function getQueryArgs()
    {
        return[
            'post_title' => $this->getDate(),
            'post_status' => 'publish',
            'post_author' => get_current_user_id(),
            'post_type' => $this->getPostType(),
            'post_name' => sanitize_title($this->getDate()),

        ];

    }

    /**
     * @return false|int|mixed|\WP_Error
     */
    public function createPost()
    {

        if ( ! get_page_by_title( $this->getDate(), OBJECT, $this->getPostType() ) ) {
            return wp_insert_post($this->getQueryArgs());
        } return false;

    }

}