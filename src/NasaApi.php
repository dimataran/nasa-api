<?php

namespace Taran\NasaApi;

class NasaApi
{

    private $options;

    /**
     * @param Options $options
     */
    public function __construct(Options $options)
    {
        $this->options = $options;
    }

    /**
     * @return Options
     */
    public function getOptions(): Options
    {
        return $this->options;
    }

    /**
     * @return string
     */
    private function getApiUrl()
    {
        return $this->getOptions()->getApiUrl();
    }

    /**
     * @return string
     */
    private function getPostPara($key = 0)
    {
        return $this->getOptions()->getPostParam($key);
    }

    /**
     * @return string
     */
    private function getApiKey()
    {
        return $this->getOptions()->getApiKey();
    }

    /**
     * @return string
     */
    private function getDataUrl()
    {
        return $this->getApiUrl() .'?' . $this->getPostPara() . '=' . $this->getApiKey();
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return string
     */
    public function getInitialUrl()
    {
        $api_url = $this->getApiUrl() .'?' . $this->getPostPara() . '=' . $this->getApiKey() . '&' . $this->getPostPara(1) . '=' . $this->getOptions()->getStartDay() . '&'
                   . $this->getPostPara(2) . '=' . $this->getOptions()->getToday();
        return $api_url;
    }

    /**
     * @return mixed
     */
    public function getJsonData($api_url)
    {
        $response = wp_remote_get($api_url, ['timeout' => 120]);
        if ($this->getCode( $response ) === 200) {
            return json_decode($this->getBody( $response ));
        }
        return false;
    }

    public function getCacheJsonData($transient_name) {
        if (($data = get_transient($transient_name)) === false) {
            if (!$data = $this->getJsonData($this->getDataUrl())) {
                $data = '';
            }
            set_transient($transient_name, $data, 60 * 60 * 24);
        }
        return $data;
    }

    public function getJsonDataWithUrl($api_url)
    {
        $response = wp_remote_get($api_url, ['timeout' => 120]);
        if ($this->getCode( $response ) === 200) {
            return json_decode($this->getBody( $response ));
        }
        return false;
    }


    public function getCacheRequestApi($transient_name) {
        if (($data = get_transient($transient_name)) === false) {
            if (!$data = $this->getJsonDataWithUrl($this->getInitialUrl())) {
                $data = '';
            }
            set_transient($transient_name, $data, 60 * 60 * 24);
        }
        return $data;
    }

    /**
     * @param $response
     * @return int|string
     */
    public function getCode($response)
    {
       return wp_remote_retrieve_response_code($response);
    }

    /**
     * @param $response
     * @param $header
     * @return string
     */
    public function getHeader($response, $header)
    {
        return wp_remote_retrieve_header($response, $header);
    }

    /**
     * @param $response
     * @return string
     */
    public function getBody($response)
    {
        return wp_remote_retrieve_body($response);
    }


}