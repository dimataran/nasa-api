<?php

namespace Taran\NasaApi;

class Options
{

    /**
     * @var string
     */
    private $post_type = 'post-nasa-gallery';

    /**
     * @var string
     * DEMO_KEY
     */
    private $api_key = 'DEMO_KEY';

    /**
     * @var string
     */
    private $api_url ='https://api.nasa.gov/planetary/apod';

    /**
     * @var array
     * start_date 	YYYY-MM-DD 	none
     * end_date 	YYYY-MM-DD 	today
     */
    private $post_param = ['api_key', 'start_date', 'end_date'];

    /**
     * @var string
     */
    private $start_day;

    /**
     * @var string
     */
    private $today;

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->api_url;
    }

    /**
     * @return string
     */
    public function getPostParam($key = 0): string
    {
        return $this->post_param[$key];
    }

    /**
     * @return string
     */
    public function getPostType(): string
    {
        return $this->post_type;
    }

    /**
     * @return false|string
     */
    public function getToday()
    {

        return date('Y-m-d');
    }

    /**
     * @return false|string
     */
    public function getStartDay()
    {
        return date('Y-m-d',   strtotime('4 days ago'));
    }


}