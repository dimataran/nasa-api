<?php

namespace Taran\NasaApi;

use Taran\NasaApi\NasaApi;

class Image extends PostType
{

    protected $gallery_id;

    protected $nasa_api;

    public function __construct(NasaApi $nasa_api, $post_data, $gallery_id)
    {
        parent::__construct($post_data);
        $this->post_type = 'attachment';
        $this->gallery_id = $gallery_id;
        $this->nasa_api = $nasa_api;
    }

    protected function getQueryArgs()
    {

        return [
            'post_title'=> $this->getDate(),
            'post_mime_type' => $this->getImgType(),
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => basename($this->post_data->url),
            'post_type' => $this->getPostType()
        ];
    }

    public function grabImg()
    {
        //upload image into wordpress
        return wp_upload_bits(urldecode(basename($this->getUrl())), '', $this->getImgBody());
    }

    /**
     * @return array|false|\WP_Error
     */
    public function getImg()
    {
            $img = wp_remote_get($this->getUrl());
            if ($this->nasa_api->getCode($img) === 200) {
                return $img;
            } else return '';
    }


    public function getImgBody()
    {
        if ($img = $this->getImg()) {
            return wp_remote_retrieve_body($img);
        } else return '';

    }


    /**
     * @return false|string
     */
    public function getImgType()
    {
        if ($img = $this->getImg()) {
            return $this->nasa_api->getHeader($img, 'content-type');
        } else return '';
    }


    public function createPost()
    {

        $attach_id = wp_insert_attachment($this->getQueryArgs(), $this->grabImg()['url'], $this->gallery_id); //insert the attachment and get the ID
        $attach_data = wp_generate_attachment_metadata($attach_id, $this->grabImg()['file']); //generate the meta-data for the image
        wp_update_attachment_metadata($attach_id, $attach_data); //update the images meta-data
        return set_post_thumbnail($this->gallery_id, $attach_id);

   }

}